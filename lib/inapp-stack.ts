import * as cdk from 'aws-cdk-lib';
import * as apigateway from '@aws-cdk/aws-apigatewayv2-alpha'
import * as integration from '@aws-cdk/aws-apigatewayv2-integrations-alpha'
import { Construct } from 'constructs';
import * as dynamodb from 'aws-cdk-lib/aws-dynamodb'
import * as lambda from 'aws-cdk-lib/aws-lambda'
import * as iam from 'aws-cdk-lib/aws-iam'
import * as stepFunction from 'aws-cdk-lib/aws-stepfunctions';
import * as cognito from 'aws-cdk-lib/aws-cognito';

export class InappStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    //--------------------------------------------------------------------------------------------
    // Customer Profile Table and Claim History Table
    //--------------------------------------------------------------------------------------------
    const customerProfileTable = new dynamodb.Table(this, 'customer-profil-table', {
      tableName: "customer-profile",
      partitionKey: { name: 'customerId', type: dynamodb.AttributeType.STRING },
    });

    const claimHistoryTable = new dynamodb.Table(this, 'claim-history-table-table', {
      tableName: "claim-history-table",
      partitionKey: { name: 'orderId', type: dynamodb.AttributeType.STRING },
      sortKey: { name: 'customerId', type: dynamodb.AttributeType.STRING }
    });

    //--------------------------------------------------------------------------------------------
    // Step Function 
    //--------------------------------------------------------------------------------------------
    const stepfunction = stepFunction.StateMachine.fromStateMachineArn(this,"stepFunction","arn:aws:states:us-east-1:834431916728:stateMachine:MyStateMachine-rzmug8gv7");
    //--------------------------------------------------------------------------------------------
    // API Lambda Handler
    //--------------------------------------------------------------------------------------------
    const myLambda = new lambda.Function(this, 'InAppLambda-1', {
      functionName: 'InAppLambda-1',
      runtime: lambda.Runtime.NODEJS_18_X,
      handler: 'index.handler',
      code: lambda.Code.fromAsset('lambda'), // Path to your Lambda code
    });

    myLambda.addToRolePolicy(
      new iam.PolicyStatement({
        resources: [customerProfileTable.tableArn, claimHistoryTable.tableArn],
        actions: ["dynamodb:GetItem", "dynamodb:PutItem"],
      })
    )

    myLambda.addToRolePolicy(
      new iam.PolicyStatement({
        resources:[stepfunction.stateMachineArn],
        actions:['*']
      })
    )

    //--------------------------------------------------------------------------------------------
    // API Gateway
    //--------------------------------------------------------------------------------------------

    const userPoolArn = 'arn:aws:cognito-idp:region:account-id:userpool/user-pool-id';
    const appClientArn = 'arn:aws:cognito-idp:region:account-id:userpool/user-pool-id/app-client/app-client-id';
    
    const api = new apigateway.HttpApi(this, "in-app-http-api", {
      apiName: "in-app-http-api-1"
    });

    const claimsIntegration = new integration.HttpLambdaIntegration("claimLambdaIntegration", myLambda, {
      payloadFormatVersion: apigateway.PayloadFormatVersion.VERSION_2_0,
    })

    const httpRoute = new apigateway.HttpRoute(this, 'resoulution/claims', {
      httpApi: api,
      integration: claimsIntegration,
      routeKey: apigateway.HttpRouteKey.with('/test', apigateway.HttpMethod.POST)
    })

    // Create an authorizer for JWT with ARNs
  const jwtAuthorizer = new apigateway.HttpAuthorizer(this, 'httpauth1',{
    httpApi:api,
    identitySource: ['Auth'],
    type: apigateway.HttpAuthorizerType.JWT,
    jwtAudience: ['1ucjarl0bcc1fv64cb9govudj1'],
    jwtIssuer: 'https://cognito-idp.us-east-1.amazonaws.com/us-east-1_w2t0Q2Fal',
    payloadFormatVersion: apigateway.AuthorizerPayloadVersion.VERSION_2_0,
    resultsCacheTtl: cdk.Duration.minutes(30),
  })
  
  
  CognitoUserPoolsAuthorizer(this, 'MyJwtAuthorizer', {
  cognitoUserPools: [cognito.UserPool.fromUserPoolArn(this, 'MyUserPool', userPoolArn)],
  cognitoUserPoolsClient: [cognito.UserPoolClient.fromUserPoolClientArn(this, 'MyAppClient', appClientArn)],
  authorizerName: 'JwtAuthorizer',
  identitySource: ['$request.header.Authorization'],
  authorizationScopes: ['scope1', 'scope2'],
});
  }
}
